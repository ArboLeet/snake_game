#ifndef GAME_H
#define GAME_H

void mainloop(
  SDL_Window* window,
  SDL_Renderer* renderer,
  struct snake* snake,
  struct segment* apple
);

#endif /* GAME_H */
