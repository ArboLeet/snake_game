#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "snake.h"
#include "game.h"
#include "config.h"

int main() {
  SDL_Init(SDL_INIT_VIDEO);
  srand(time(NULL));

  SDL_Window* window = SDL_CreateWindow(
    "Snake score: 0",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    FIELD_WIDTH * TILE_SIZE,
    FIELD_HEIGHT * TILE_SIZE,
    0
  );

  SDL_Renderer* renderer = SDL_CreateRenderer(
    window,
    -1,
    SDL_RENDERER_ACCELERATED
  );

  // Start game
  struct snake snake = {
    .body = malloc(sizeof(struct segment)),
    .length = 1,
    .dir = DIR_RIGHT
  };
  snake.body[0] = (struct segment){.y = FIELD_HEIGHT / 2};

  struct segment apple = {.x = FIELD_WIDTH / 2, .y = FIELD_HEIGHT / 2};

  mainloop(window, renderer, &snake, &apple);

  printf("Score: %zu\n", snake.length - 1);

  // Close program
  free(snake.body);

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
