#ifndef SNAKE_H
#define SNAKE_H

#include <stdbool.h>

struct segment {
  size_t x, y;
};

enum direction {
  DIR_UP,
  DIR_LEFT,
  DIR_DOWN,
  DIR_RIGHT
};

struct snake {
  struct segment* body;
  size_t length;
  enum direction dir;
};

void moveSnake(struct snake* snake);
bool eatApple(struct snake* snake, struct segment* apple);

bool tailContains(const struct snake* snake, const struct segment* segment);

#endif /* SNAKE_H */
